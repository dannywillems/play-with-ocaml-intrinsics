let with_cpu_cycles_and_time f =
  let start_cpu = Ocaml_intrinsics.Perfmon.rdtsc () in
  let start_time = Core.Time_ns.now () in
  let res = f () in
  let end_cpu = Ocaml_intrinsics.Perfmon.rdtsc () in
  let end_time = Core.Time_ns.now () in
  ( res,
    Int64.sub end_cpu start_cpu,
    Core.Time_ns.Span.to_string_hum @@ Core.Time_ns.abs_diff end_time start_time
  )

let rdtsc_add_bls12_381_fr () =
  let x = Bls12_381.Fr.random () in
  let y = Bls12_381.Fr.random () in
  let _, diff_cpu, diff_time =
    with_cpu_cycles_and_time (fun () -> Bls12_381.Fr.add x y)
  in
  Printf.printf
    "Bench for Bls12_381.Fr.add: %s CPU cycle - time = %s\n"
    (Int64.to_string diff_cpu)
    diff_time

let rdtsc_pairing_bls12_381 () =
  let x = Bls12_381.G1.random () in
  let y = Bls12_381.G2.random () in
  let _, diff_cpu, diff_time =
    with_cpu_cycles_and_time (fun () -> Bls12_381.Pairing.pairing x y)
  in
  Printf.printf
    "Bench for Bls12_381.Pairing.pairing: %s CPU cycle - time: %s\n"
    (Int64.to_string diff_cpu)
    diff_time

let sign_and_verify_ed25519_hacl_star () =
  let sk = Option.get @@ Hacl_star.Hacl.RandomBuffer.randombytes ~size:32 in
  let pk = Hacl_star.Hacl.Ed25519.secret_to_public ~sk in
  let msg = Option.get @@ Hacl_star.Hacl.RandomBuffer.randombytes ~size:32 in
  let signature, sign_cpu, diff_time_sig =
    with_cpu_cycles_and_time (fun () -> Hacl_star.Hacl.Ed25519.sign ~sk ~msg)
  in
  let verif, verif_cpu, diff_time_verif =
    with_cpu_cycles_and_time (fun () ->
        Hacl_star.Hacl.Ed25519.verify ~pk ~msg ~signature)
  in
  Printf.printf
    "Bench for Hacl_star.Hacl.Ed25519.sign: %s CPU cycle - time %s\n"
    (Int64.to_string sign_cpu)
    diff_time_sig ;
  Printf.printf
    "Bench for Hacl_star.Hacl.Ed25519.verify: %s CPU cycle - time %s\n"
    (Int64.to_string verif_cpu)
    diff_time_verif ;
  assert verif

let bench_hash_function name fn =
  let input = Option.get @@ Hacl_star.Hacl.RandomBuffer.randombytes ~size:32 in
  let _, cpu_time, _ = with_cpu_cycles_and_time (fun () -> fn input) in
  Printf.printf "Diff CPU for %s: %s\n" name (Int64.to_string cpu_time)

let bench_hash_function_noalloc name fn output_size input_size =
  let input =
    Option.get @@ Hacl_star.Hacl.RandomBuffer.randombytes ~size:input_size
  in
  let output =
    Option.get @@ Hacl_star.Hacl.RandomBuffer.randombytes ~size:output_size
  in
  let _, cpu_time, diff_time =
    with_cpu_cycles_and_time (fun () -> fn input output)
  in
  Printf.printf
    "Bench for %s: %s CPU cycle - time %s\n"
    name
    (Int64.to_string cpu_time)
    diff_time

let () =
  bench_hash_function "SHA256" Hacl_star.Hacl.SHA2_256.hash ;
  bench_hash_function "Blake2b" (fun msg ->
      Hacl_star.Hacl.Blake2b_32.hash msg 32) ;
  bench_hash_function_noalloc
    "SHA256 noalloc"
    (fun input output ->
      Hacl_star.Hacl.SHA2_256.Noalloc.hash ~msg:input ~digest:output)
    32
    32 ;
  rdtsc_add_bls12_381_fr () ;
  sign_and_verify_ed25519_hacl_star () ;
  rdtsc_pairing_bls12_381 ()
