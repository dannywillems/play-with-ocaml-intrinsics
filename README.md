Test ocaml-intrinsics
==========================


See [ocaml-intrinsics](https://github.com/janestreet/ocaml_intrinsics)

```shell
opam switch create ./ 4.14.0 --no-install
eval $(opam env)
opam install ocamlformat.0.20.1 merlin ocaml_intrinsics
```
